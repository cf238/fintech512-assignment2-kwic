import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.io.*;
import java.util.*;


public class Kwictest {
    private final InputStream standardIn = System.in;
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    Set<String> words = new HashSet<>();
    List<String> titles = new ArrayList<>();
    Kwic kwic = new Kwic();

    @BeforeEach
    public void setUp() {
        try {
            System.setIn(new FileInputStream("../resources/input"));
            System.setOut(new PrintStream(outputStreamCaptor));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        kwic.readWordsAndTitles(words, titles);
    }

    @AfterEach
    public void tearDown() {
        System.setIn(standardIn);
        System.setOut(standardOut);
    }

    @Test
    public void testReadWordsAndTitles() {
        int expectedWordsSize = 7, expectedTitlesSize = 5;
        assertEquals(expectedWordsSize, words.size());
        assertTrue(words.contains("is"));
        assertTrue(words.contains("the"));
        assertTrue(words.contains("of"));
        assertTrue(words.contains("and"));
        assertTrue(words.contains("as"));
        assertTrue(words.contains("a"));
        assertTrue(words.contains("but"));


        String[] expectedTitles = {
                "Descent of Man",
                "The Ascent of Man",
                "The Old Man and The Sea",
                "A Portrait of The Artist As a Young Man",
                "A Man is a Man but Bubblesort IS A DOG" };

        assertEquals(expectedTitlesSize, titles.size());
        for (int i = 0; i < expectedTitlesSize; i++) {
            assertEquals(expectedTitles[i], titles.get(i));
        }
    }

    @Test
    // identify keyword
    public void testGetKeywords() {

        Set<String> keywords = kwic.getKeyWords(words, titles.get(0));
        int expectedSize = 2;
        assertEquals(expectedSize, keywords.size());
        assertTrue(keywords.contains("descent"));
        assertTrue(keywords.contains("man"));

        keywords = kwic.getKeyWords(words, titles.get(1));
        expectedSize = 2;
        assertEquals(expectedSize, keywords.size());
        assertTrue(keywords.contains("ascent"));
        assertTrue(keywords.contains("man"));

        keywords = kwic.getKeyWords(words, titles.get(2));
        expectedSize = 3;
        assertEquals(expectedSize, keywords.size());
        assertTrue(keywords.contains("old"));
        assertTrue(keywords.contains("man"));
        assertTrue(keywords.contains("sea"));

        keywords = kwic.getKeyWords(words, titles.get(3));
        expectedSize = 4;
        assertEquals(expectedSize, keywords.size());
        assertTrue(keywords.contains("portrait"));
        assertTrue(keywords.contains("artist"));
        assertTrue(keywords.contains("young"));
        assertTrue(keywords.contains("man"));

        keywords = kwic.getKeyWords(words, titles.get(4));
        expectedSize = 3;
        assertEquals(expectedSize, keywords.size());
        assertTrue(keywords.contains("man"));
        assertTrue(keywords.contains("bubblesort"));
        assertTrue(keywords.contains("dog"));
    }


    @Test
    // Print all titles with one upper-case keyword at a time and sorted titles alphabetically based on keywords
    public void testPrintAllTitles() {
        StringBuilder expected = new StringBuilder();

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("../resources/output"));
            String line = reader.readLine();
            while (line != null) {
                expected.append(line).append("\r\n");
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        kwic.printAllTitles(words, titles);


        assertEquals(expected.toString(), outputStreamCaptor.toString());

    }

}

