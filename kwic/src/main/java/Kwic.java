import java.util.*;

public class Kwic {
    public static void readWordsAndTitles(Set<String> words, List<String> titles) {
        Scanner scan = new Scanner(System.in);
        //Read ignore words
        while (scan.hasNextLine()) {
            String line = scan.nextLine();
            if (line.equals("::")) {
                break;
            }
            words.add(line);
        }

        //Read titles
        while (scan.hasNextLine()) {
            String line = scan.nextLine();
            titles.add(line);
        }
    }

    public Set<String> getKeyWords(Set<String> wordsToIgnore, String title) {
        String[] words = title.toLowerCase().split(" ");
        Set<String> keywords = new HashSet<>();
        for (String word : words) {
            if (wordsToIgnore.contains(word)) {
                continue;
            }
            keywords.add(word);
        }
        return keywords;
    }

    public void printTitle(String title, String keyword) {
        title = title.toLowerCase();
        int idx = title.indexOf(keyword, 0);
        while (idx != -1) {
            StringBuilder sb = new StringBuilder();
            sb.append(title, 0, idx);
            sb.append(keyword.toUpperCase());
            sb.append(title, idx + keyword.length(), title.length());
            System.out.println(sb);
            idx = title.indexOf(keyword, idx + keyword.length());
        }
    }

    public void printAllTitles(Set<String> wordsToIgnore, List<String> titles){
        TreeMap<String, List<Integer>> titleKey = new TreeMap<>();
        for (int i = 0; i < titles.size(); i++){
            Set<String> keys = getKeyWords(wordsToIgnore, titles.get(i));
            for (String key : keys){
                if (!titleKey.containsKey(key)) {
                    titleKey.put(key, new ArrayList<>());
                }
                titleKey.get(key).add(i);

            }
        }

        for (Map.Entry<String, List<Integer>> entry : titleKey.entrySet()){
            String word = entry.getKey();
            List<Integer> titleIndex = entry.getValue();
            for (Integer idx : titleIndex){
                printTitle(titles.get(idx), word);
            }
        }
    }
}
